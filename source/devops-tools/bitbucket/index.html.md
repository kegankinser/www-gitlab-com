---
layout: markdown_page
title: "Atlassian Bitbucket"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Atlassian Bitbucket gives teams Git code management, but also one place to plan projects, collaborate on code, test and deploy. It is marketed in the SaaS form (Bitbucket Cloud) and in a self-managed version (Bitbucket Server), however they are not the same product. Bitbucket Server is simply a [re-branding of Stash](https://www.atlassian.com/blog/archives/atlassian-stash-enterprise-git-repository-management). The two products are completely different code bases, written in two different languages ([Cloud in Python, Server in Java](https://en.wikipedia.org/wiki/Bitbucket)) and do not maintain feature parity. Because of separate codebases they each have a completely different API, making it much harder to integrate.

Bitbucket supports  Mercurial or Git, but not SVN. GitLab does not support Mercurial or SVN.

GitLab is a single application for the complete DevOps lifecycle with built-in project management, source code managemetn, CI/CD, monitoring and more. Bitbucket only does source code managment. You would need to use Atlassian Jira to get project managment, and Bamboo for CI/CD and Atlassian does not provide a monitoring solution. Addditionally, GitLab Ultimate comes with robust built-in security capabilities such as SAST, DAST, Container Scanning, Dependency Scanning, and more. Bitbucket does not support these capabilities, and Atlassian does not have a product for them.

GitLab also offers a "prem" self-managed and "cloud" SaaS solution. GitLab runs the same exact code on it's SaaS platform that it offers to it's customers. This means customers can migrated from self-hosted to SaaS and back relatively easily and each solution maintains feature parity.

## Weaknesses
* Extending the native functionality of Bitbucket is done through plugins. Plugins are expensive to maintain, secure, and upgrade. In contrast, GitLab is [open core](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change. 

## Comments/Anecdotes
* Evidence that Bitbucket Cloud and Bitbucket Server/Data Center are two different products and Atlassian is focused on the cloud one:
   * From Atlassian themselves, Bitbucket Cloud and Bitbucket Server are different architectures (ie. code bases) and diverging in functionality. This is more or less spelled out in their [Bitbucket Rebrand FAQ (updated Jan 2018)](https://confluence.atlassian.com/bitbucketserver/bitbucket-rebrand-faq-779298912.html?_ga=2.68100121.980444013.1542263978-1592705151.1523946360)
   * From a recent [HackerNews article](https://news.ycombinator.com/item?id=17909454)
   >Atlassian wants you to move to their cloud product, it's that simple. Server is a thing that makes their life harder and stops them from extracting maximum revenues from you so you can expect a soft but persistent push to switch to cloud which will get harder over time.
   * Disparity between Bitbucket Server and Bitbucket Cloud for a top customer requested feature took Atlassian over 2 years to acknowledge. Customers found out the feature existed in Server but not Cloud once moving to Cloud. [Still not resolved as of Q4 2018](https://bitbucket.org/site/master/issues/12833/branching-models-for-bb-cloud#comment-45982415)
   * [An analysis of Bitbucket Server](https://docs.google.com/spreadsheets/d/1BnxRkNKytRcEnKqlnCyJFy_B6swjGyLdcwGi9bg32PY/edit#gid=0) new features per release and feature-to-maintenance ratio, per release since it was changed from Stash Server, shows a downward trend in innovation ([Data derived from Atlassian Bitbucket Server Release Notes](https://confluence.atlassian.com/alldoc/bitbucket-server-documentation-directory-278071997.html))
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <body><center>
         <a href="/devops-tools/bitbucket/bitbucket_new_feature_graph.png" target="_blank">
          <img src="/devops-tools/bitbucket/bitbucket_new_feature_graph.png" alt="Bitbucket Server New Features Graph" style="width:65%;">
         </a>
      </center></body>

* Discussion from [HackerNews article about Atlassian not allowing benchmarking](https://news.ycombinator.com/item?id=18103162#18103813)
   > Atlassian has always forbidden to talk about the performance of their products in their ToS and in their previous EULA. We all know why, but we don’t talk about it.


## Resources
* [Atlassian Bitbucket Website](https://bitbucket.org/product)

## Pricing
- [Bitbucket Cloud](https://bitbucket.org/product/pricing?tab=#tab-5834cfd9-fbc0-4f21-ae10-65832598b12a)
  * Free tier - $0 - Unlimited private repos, Jira Software integration, Projects Pipelines (50 build mins/month), 1GB/month limit on file storage
  * Standard tier - $2/user/month (min $10/month) - Same as Free + 500 build mins/month + 5GB file storage/month
  * Premium tier - $5/usr/month (min $25/month) - Standard + some advanced features + 1000 build mins/month + 10GB file storage/month

- [Bitbucket Server / Data Center](https://bitbucket.org/product/pricing?tab=#tab-db2f88e8-2388-4417-8e4f-6a7205437cc7)
  * Server - starting $2k perpetual (25 users, ppu drops roughly every 2x previous tier), includes year maintenance, single server, unlimited priv+pub repos
  * Data Center - $1800/yr (25 users , ppu drops roughly every 2x previous tier) includes annual maintenance, Server + HA, DR, mirroring, SAML 2.0
  * Must buy Data Center if over 2k users.

## Comparison
